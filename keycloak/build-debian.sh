#!/bin/bash
set -e

# shellcheck disable=SC1091
source ../function.sh

VERSION_EOLE="${VERSION}-debian-eole3.1"
DOCKERFILE="Dockerfile-debian"
LOCAL_TAG="keycloack:dev-debian"
VERSION=$(awk '/ENV KEYCLOAK_VERSION / {print $3;}' <"$DOCKERFILE")
if [ -z "$VERSION" ]
then
  exit 1
fi

VERSION_EOLE="${VERSION}-debian-eole3.1"
BASE_IMAGE="hub.eole.education/test/keycloak"
IMAGE="$BASE_IMAGE:$VERSION_EOLE"

loginHarbor

export BUILDKIT_PROGRESS=plain
#  --no-cache 
if ! docker build -f "$DOCKERFILE" . -t "$LOCAL_TAG" 
then
  exit 1
fi

SIZE=$(docker inspect "$LOCAL_TAG"  | jq '.[] | .Size')
echo "TAILLE=$SIZE"

if ! docker tag "$LOCAL_TAG" "$IMAGE"
then
  exit 1
fi

if ! docker push "$IMAGE"
then
  exit 1
fi

echo "Pour tester:"
echo "docker run -p 8080:8080 -e DB_VENDOR=H2 -e KEYCLOAK_USER=gilles -e KEYCLOAK_PASSWORD=gilles -e KEYCLOAK_ADMIN=admin -e KEYCLOAK_ADMIN_PASSWORD=admin $LOCAL_TAG start-dev "
