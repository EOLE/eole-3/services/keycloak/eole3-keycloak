source /functions.sh

echo "KEYCLOAK_USER=$KEYCLOAK_USER"
echo "KEYCLOAK_PASSWORD=$KEYCLOAK_PASSWORD"
echo "KEYCLOAK_URL=$KEYCLOAK_URL"
echo "KC_PUBLIC_HOSTNAME_PUBLIC=$KC_PUBLIC_HOSTNAME_PUBLIC"
echo "KC_REALM_NAME=$KC_REALM_NAME"
echo "KC_REALM_DISPLAY_NAME=$KC_REALM_DISPLAY_NAME"
echo "KC_REALM_USERNAME=$KC_REALM_USERNAME"
echo "KC_REALM_PASSWORD=$KC_REALM_PASSWORD"
echo "KC_API_CLIENT_ID=$KC_API_CLIENT_ID"
echo "KC_API_CLIENT_HOSTNAME_PUBLIC=$KC_API_CLIENT_HOSTNAME_PUBLIC"
    
waitUrl "$KEYCLOAK_URL"
    
# cf. https://github.com/keycloak/keycloak-documentation/blob/master/server_admin/topics/admin-cli.adoc    
realm=$KC_REALM_NAME
if [ -z "$realm" ]
then
    echo "Realm not set. Beware to call this script with Make!"
    exit 1
fi

if ! loginKeycloack
then
    exit $?
fi

if ! createRealm
then
    exit $?
fi

#########################################
# Create client(s)
#########################################
$kcadm get clients -r $realm >$output/clients.json
#cat $output/clients.json
    
client_id=$(jq '.[] | select(.clientId == "'$KC_API_CLIENT_ID'") | .id' -r $output/clients.json)
echo "ID for '$KC_API_CLIENT_ID' = $client_id"
set -x  
if [ -z "$client_id" ]
then
    client_id=$($kcadm create clients \
      -r $realm \
      -s clientId=$KC_API_CLIENT_ID \
      -s baseUrl=$KC_API_CLIENT_HOSTNAME_PUBLIC \
      -s "redirectUris=[\"$KC_API_CLIENT_HOSTNAME_PUBLIC/*\"]" \
      -s "webOrigins=[\"+\"]" \
      -i)
    CDU="$?"
    if [ "$CDU" -ne 0 ]
    then
        echo "Unable to create client"
    else
        echo "Client '$client_id' created."
        $kcadm get clients -r $realm >$output/clients.json
    fi
fi
set +x
exportRealmKeys

#########################################
# Create roles
#########################################
getRoles
createRole "ROLE_ADMIN_ZEPHIR" "description=Regular admin with full set of permissions"
createRole "ROLE_OPERATOR_ZEPHIR" "description=Regular operator with basic set of permissions"
createRole "ROLE_INSTALLER_MODULE" "description=Regular technician with only enrollement permissions"

#########################################
# Create groups
#########################################
getGroups
GROUP_ID=""
createGroup "AdminZephir"
gidAdmin=$GROUP_ID
echo gidAdmin=$gidAdmin 
createGroup "OperatorZephir"
gidOperator=$GROUP_ID
echo gidOperator=$gidOperator
createGroup "InstallerZephir"
gidInstaller=$GROUP_ID
echo gidInstaller=$gidInstaller

#########################################
# Role affectation
#########################################
#affectGroupToRole "AdminZephir" "ROLE_ADMIN_ZEPHIR"
#affectGroupToRole "OperatorZephir" "ROLE_OPERATOR_ZEPHIR"
#affectGroupToRole "InstallerZephir" "ROLE_INSTALLER_MODULE"

#########################################
# Create users
#########################################
getUsers
USER_UID=""
createUser "$KC_REALM_USERNAME" "$KC_REALM_PASSWORD" "$gidAdmin"
uid=$USER_UID

#########################################
# Getting adapter configuration file
#########################################
echo "Get adapter configuration file..."
$kcadm get clients/$client_id/installation/providers/keycloak-oidc-keycloak-json \
  -r $realm \
  | jq ".[\"auth-server-url\"]=\"$KC_PUBLIC_HOSTNAME_PUBLIC/auth\"" \
  > $output/keycloak.json
CDU="$?"
if [ "$CDU" -ne 0 ]
then
    echo "Unable to get configuration file"
else
    cat $output/keycloak.json
    
    SECRET=$(jq -r .credentials.secret $output/keycloak.json)
    echo "SECRET=$SECRET"
    
    echo "Keycloak successfully configured."
fi

cp /opt/jboss/keycloak/keycloak.json /opt/jboss/keycloak/exportkeys/keycloak.json
cp /opt/jboss/keycloak/pub.pem /opt/jboss/keycloak/exportkeys/pub.pem
cp /opt/jboss/keycloak/cert.pem /opt/jboss/keycloak/exportkeys/cert.pem
ls -l /opt/jboss/keycloak/exportkeys/

