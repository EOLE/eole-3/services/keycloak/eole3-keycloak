die() { echo "$@" 1>&2 ; exit 1; }

function waitUrl()
{
    local URL="$1"
    local NB="${2:-30}"
    #echo "waitUrl: $URL"
    while [ "${NB}" -gt 0 ] ; do
      if curl --output /tmp/curl --silent --head "$URL"
      then
          HTTPCODE=$(awk 'NR==1 { print $2; }' /tmp/curl)
      else
          HTTPCODE="-1"
      fi
           
      if [ "$HTTPCODE" = "200" ] || [ "$HTTPCODE" = "303" ] #|| [ "$HTTPCODE" = "404" ]
      then
          return 0
      fi
      sleep 1
      NB=$(( NB - 1))
    done
    cat /tmp/curl
    return 1
}

function getRoles()
{
    # --clientid $KC_API_CLIENT_ID
    $kcadm get roles -r $realm  >$output/roles.json
    #cat $output/roles.json
}

function createRole()
{
    local ROLE_NAME="$1"
    local ROLE_DESC="$2"
    
    # attention GROUP_ID = global var
    ROLE_ID=$(jq '.[] | select(.name == "'$ROLE_NAME'") | .id' -r $output/roles.json)
    if [ -n "$ROLE_ID" ]
    then
        echo "Role $ROLE_NAME already exists with id $ROLE_ID"
        return 0
    else
        # --clientid $KC_API_CLIENT_ID
        $kcadm create roles -r $realm -s name="$ROLE_NAME" -s "$ROLE_DESC"
        CDU="$?"
        if [ "$CDU" -ne "0" ] 
        then
            echo "Unable to create '$ROLE_NAME' role"
            return 1
        else
            echo "Role $ROLE_NAME created."
            getRoles
            ROLE_ID=$(jq '.[] | select(.name == "'$ROLE_NAME'") | .id' -r $output/roles.json)
        fi
    fi
    return 0
}

function affectGroupToRole()
{
    local GROUP_NAME="$1"
    local ROLE_NAME="$2"
    # --clientid $KC_API_CLIENT_ID
    $kcadm add-roles -r $realm --gname "$GROUP_NAME" --rolename "$ROLE_NAME" 
    CDU="$?"
    if [ "$CDU" -ne "0" ]
    then
        echo "Unable to affect '$GROUP_NAME' role to the '$ROLE_NAME' group"
        return 1
    else
        echo "Group $GROUP_NAME affected to $ROLE_NAME"
        return 0
    fi
}

function getGroups()
{
    $kcadm get groups -r $realm >$output/groups.json
    #cat $output/groups.json
}

function createGroup()
{
    local GROUP_NAME="$1"

    # attention GROUP_ID = global var
    GROUP_ID=$(jq '.[] | select(.name == "'$GROUP_NAME'") | .id' -r $output/groups.json)
    
    if [ -n "$GROUP_ID" ]
    then
        echo "Group $GROUP_NAME already exists with ID = $GROUP_ID"
        return 0
    else
        echo "Group $GROUP_NAME to be created."
        $kcadm create groups -r $realm -s name="$GROUP_NAME" -i 
        CDU="$?"
        if [ "$CDU" -ne "0" ]
        then
            echo "Unable to create '$GROUP_NAME' Group"
            return 1
        else
            echo "Group $GROUP_NAME created."
            getGroups
            GROUP_ID=$(jq '.[] | select(.name == "'$GROUP_NAME'") | .id' -r $output/groups.json)
        fi
    fi
}

function getUsers()
{
    $kcadm get users -r $realm >$output/users.json
    #cat $output/users.json
}

function createUser()
{
    local USERNAME="$1"
    local PASSWORD="$2"
    local GROUP_ID="$3"
    
    #########################################
    # Create users
    #########################################
    # attention USER_UID = global var
    USER_UID=$(jq '.[] | select(.username == "'$USERNAME'") | .id' -r $output/users.json)
    echo "UID for '$USERNAME' = $USER_UID"
    if [ -n "$USER_UID" ]
    then
        echo "User $USERNAME already exists with ID = $USER_UID"
        return 0
    else
        $kcadm create users -r "$realm" -s username="$USERNAME" -s enabled=true -i
        getUsers
        USER_UID=$(jq '.[] | select(.username == "'$USERNAME'") | .id' -r $output/users.json)
        echo "UID for '$USERNAME' = $USER_UID"
        
        $kcadm update users/$USER_UID/reset-password \
          -r $realm \
          -s type=password \
          -s value="$PASSWORD" \
          -s temporary=false \
          -n
        CDU="$?"
        if [ "$CDU" -ne "0" ]
        then
            echo "Unable to set '$USERNAME' password"
        else
            echo "password '$USERNAME' reset"
        fi
    
        if [ -n "$GROUP_ID" ]
        then
            #########################################
            # Group affectations
            #########################################
            $kcadm update users/$USER_UID/groups/$GROUP_ID \
                 -r $realm \
                 -s realm=$realm \
                 -s userId=$USER_UID \
                 -s groupId=$GROUP_ID \
                 -n
            CDU="$?"
            if [ "$CDU" -ne 0 ]
            then
                echo "Unable to affect '$USER_UID' user to the '$GROUP_ID' group"
            else
                echo "$USERNAME user affected to the '$GROUP_ID' group."
            fi
        fi
    fi
}

function loginKeycloack()
{
    # see : http://www.keycloak.org/docs/3.1/server_admin/topics/admin-cli.html
    #/opt/jboss/keycloak/bin/kcadm.sh config credentials --server http://localhost:8080/auth --realm master --user admin --password admin
    $kcadm config credentials --server $KEYCLOAK_URL --realm master --user $KEYCLOAK_USER --password $KEYCLOAK_PASSWORD
    if [ "$?" -ne 0 ]
    then
        echo "Unable to login"
        return 1
    else
        echo "logged as $KEYCLOAK_USER"
        return 0
    fi
}

function createRealm()
{
    $kcadm get realms/$realm >$output/realm.json
    if [ $? = 0 ]
    then
        echo "Realm '$realm' already exists."
        return 0
    else
        echo "Realm '$realm' to be created."

        #########################################
        # Create realm
        #########################################
        REALM_ID=$($kcadm create realms -s realm=$realm -s enabled=true -i)
        if [ "$?" -ne 0 ]
        then
            echo "Unable to create realm"
            return 2
        else
            echo "Realm '$REALM_ID' created."
            $kcadm get realms/$realm >$output/realm.json
        fi
    fi
    
    $kcadm update realms/$realm -s registrationAllowed=true -s rememberMe=true
    if [ "$?" -ne 0 ]
    then
        echo "Unable to configure realm"
        return 3
    else
        echo "Realm '$REALM_ID' configured."
    fi
    
    return 0
}

function getClients()
{
    $kcadm get clients -r $realm >$output/clients.json
    #cat $output/clients.json
}

function exportRealmKeys()
{
    echo "clean ouput keys..."
    rm $output/{*.pem,*.json} 2> /dev/null
    [ $? = 0 ] && echo "Output directory cleaned!"
    
    echo "Get realm keys..."
    $kcadm get keys -r $realm >$output/keys.json
    [ $? = 0 ] || die "Unable to get realm keys"
    cat $output/keys.json

    jq '.keys[] | select(.algorithm == "RS256" )' -r $output/keys.json >$output/keys_rs256.json
    cat $output/keys_rs256.json
    
    jq ".publicKey" -r $output/keys_rs256.json > $output/pub.tmp
    sed -e "1 i -----BEGIN PUBLIC KEY-----" -e "$ a -----END PUBLIC KEY-----" $output/pub.tmp > $output/pub.pem
    cat $output/pub.pem
    rm $output/pub.tmp
    jq ".certificate" -r $output/keys_rs256.json > $output/cert.tmp
    sed -e "1 i -----BEGIN CERTIFICATE-----" -e "$ a -----END CERTIFICATE-----" $output/cert.tmp > $output/cert.pem
    cat $output/cert.pem
    rm $output/cert.tmp
}

kcadm=$JBOSS_HOME/bin/kcadm.sh
output=/opt/jboss/keycloak
mkdir -p "$output"
