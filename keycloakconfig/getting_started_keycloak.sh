#!/bin/bash

source getting_started_function.sh


# https://ncarlier.gitbooks.io/oss-api-management/content/howto-kong_with_keycloak.html
echo "Check https://httpbin.org/get"
curl -s -i --output /tmp/curl https://httpbin.org/get
testCurl "200"

echo "Delete API test"
(curl -s -i --output /tmp/curl -X DELETE --url $KONG_API_URL/apis/test ; exit 0)
testCurl "200" "204" "404"

echo "Add api 'test'"
curl -s -i --output /tmp/curl -X POST \
  --url $KONG_API_URL/apis/ \
  --data 'name=test' \
  --data 'upstream_url=https://httpbin.org/' \
  --data 'uris=/test' \
  --data 'strip_uri=true'
testCurl "201"

echo "test api 'test' (/get de httpbin !)"
curl -s -i --output /tmp/curl -X GET \
  --url $KONG_API_URL/apis/test \
  --header "Host: $HOSTNAME_PUBLIC"
testCurl "200"

echo "test api 'test' (/get de httpbin !)"
curl -s -i --output /tmp/curl -X GET \
  --url $PUBLIC_URL/test/get \
  --header "Host: $HOSTNAME_PUBLIC"
curlToJson
testCurl "200"

SECRET=$(jq -r .credentials.secret ./keycloak.json)
echo "SECRET=$SECRET"

echo "Now we are able to add a plugin to the new API. Let's activate the OIDC plugin:"
curl -s -i --output /tmp/curl -X POST \
  --url $KONG_API_URL/apis/test/plugins/ \
  --data "name=oidc" \
  --data "config.discovery=http://$IP_PUBLIC:8080/auth/realms/zephir/.well-known/openid-configuration" \
  --data "config.client_id=zephir-api" \
  --data "config.client_secret=$SECRET"
  #--data 'config.secret_is_base64=true'
curlToJson
testCurl "201"
TEST_ID=$(jq -r .id /tmp/json)

echo "Remove TestKeycloak through the RESTful API"
( curl -s -i --output /tmp/curl -X DELETE --url $KONG_API_URL/consumers/TestKeycloak ; exit 0)
testCurl "200" "204" "404"

echo "Create TestKeycloak through the RESTful API"
curl -s -i --output /tmp/curl -X POST \
  --url $KONG_API_URL/consumers/ \
  --data "username=TestKeycloak"
testCurl "201"
curlToJson
CONSUMER_ID=$(jq -r .id /tmp/json)


echo "Verify that TestKeycloak can't acces at mockbin"
curl -s -i --output /tmp/curl -X GET \
  --url $PUBLIC_URL/test/get \
  --header 'Host: api.example.org'
testCurl "401"

JWT=""
iat="$(date +%s)"
exp=$(( iat + 31536000 )) # 365*3600*24
createJWT_Secret "${KONG_USER_JWT_SECRET}" \
                     "{'typ':'JWT','alg':'HS256'}" \
                     "{ 'iss':'$KONG_USER_JWT_ISSUER','iat':'$iat','nbf':'$iat','exp':'$exp' }"
echo "$JWT"
checkJWT "${KONG_USER_JWT_SECRET}" "$JWT"

echo "Verify that TestKeycloak can acces at test with key"
curl -s -i --output /tmp/curl -X GET \
  --url $PUBLIC_URL/get \
  --header "Host: api.example.org" \
  --header "Authorization: Bearer $JWT"
testCurl "200"




#--data "name=external-oauth" \
#    --data "config.authorize_url=https://oauth.something.net/openid-connect/authorize" \
#    --data "config.scope=openid+profile+email" \
#    --data "config.token_url=https://oauth.something.net/openid-connect/token" \
#    --data "config.client_id=SOME_CLEINT_ID" \
#    --data "config.client_secret=SOME_SECRET_KEY" \
#    --data "config.user_url=https://oauth.something.net/openid-connect/userinfo" \
#    --data "config.user_keys=email,name,sub"
#    --data "config.hosted_domain=mycompany.com"
#    --data "config.email_key=email"

#echo "Once created we add RS256 JWT credentials to the consumer."
#TOKEN_ISSUER="my-token-issuer"
#RSA_PUB_KEY=$(cat mykey-pub.pem)
#curl -s -i --output /tmp/curl -X POST \
#  --url $KONG_API_URL/consumers/$CONSUMER_ID/jwt/ \
#  --data "key=$TOKEN_ISSUER" \
#  --data "algorithm=RS256" \
#  --data-urlencode "rsa_public_key=$RSA_PUB_KEY"
#curlToJson
#KONG_USER_JWT_ID=$(jq -r .id /tmp/json)
#KONG_USER_JWT_SECRET=$(jq -r .secret /tmp/json)
#KONG_USER_JWT_ISSUER=$(jq -r .key /tmp/json)
#echo "KONG_USER_JWT_ID=$KONG_USER_JWT_ID"
#echo "KONG_USER_JWT_SECRET=$KONG_USER_JWT_SECRET"
#echo "KONG_USER_JWT_ISSUER=$KONG_USER_JWT_ISSUER"
#
##Note:
##  the usage of data-urlencode in the command to properly encode the public key
##  the key parameter set to the ID of the token issuer. This key is used to validate the issuer field of the token.
#testCurl "201"
