die() { echo "$@" 1>&2 ; exit 1; }

function waitUrl()
{
    local URL="$1"
    local NB="${2:-30}"
    #echo "waitUrl: $URL"
    while [ "${NB}" -gt 0 ] ; do
      if curl --output /tmp/curl --silent --head "$URL"
      then
          HTTPCODE=$(awk 'NR==1 { print $2; }' /tmp/curl)
      else
          HTTPCODE="-1"
      fi
           
      if [ "$HTTPCODE" = "200" ] || [ "$HTTPCODE" = "303" ] #|| [ "$HTTPCODE" = "404" ]
      then
          return 0
      fi
      sleep 1
      NB=$(( NB - 1))
    done
    cat /tmp/curl
    return 1
}

function createRole()
{
    local ROLE_NAME="$1"
    local ROLE_DESC="$2"
    $kcadm create roles -r $realm -s name="$ROLE_NAME" -s "$ROLE_DESC"
    [ $? = 0 ] || die "Unable to create '$ROLE_NAME' role"
    echo "Role $ROLE_NAME created."
}

function affectGroupToRole()
{
    local GROUP_NAME="$1"
    local ROLE_NAME="$2"
    $kcadm add-roles -r $realm --gname "$GROUP_NAME" --rolename "$ROLE_NAME"
    [ $? = 0 ] || die "Unable to affect '$GROUP_NAME' role to the '$ROLE_NAME' group"
    echo "Group $GROUP_NAME affected to $ROLE_NAME"
}

function createGroup()
{
    local GROUP_NAME="$1"
    GROUP_ID=$($kcadm create groups -r $realm -s name="$GROUP_NAME" -i)
    [ $? = 0 ] || die "Unable to create '$GROUP_NAME' group"
    echo "Group $GROUP_NAME created with gid $GROUP_ID"
}

function createUser()
{
    local USERNAME="$1"
    local PASSWORD="$2"
    local GROUP_ID="$3"
    
    #########################################
    # Create users
    #########################################
    USER_UID=$($kcadm create users -r "$realm" -s username="$USERNAME" -s enabled=true -i)
    [ $? = 0 ] || die "Unable to create '$USERNAME' user"
    
    $kcadm update users/$USER_UID/reset-password \
      -r $realm \
      -s type=password \
      -s value="$PASSWORD" \
      -s temporary=false \
      -n
    [ $? = 0 ] || die "Unable to set '$USERNAME' password"
    
    echo "User '$USERNAME' created with UID=$USER_UID"
    
    if [ -n "$GROUP_ID" ]
    then
        #########################################
        # Group affectations
        #########################################
        $kcadm update users/$USER_UID/groups/$GROUP_ID \
             -r $realm \
             -s realm=$realm \
             -s userId=$USER_UID \
             -s groupId=$GROUP_ID \
             -n
        [ $? = 0 ] || die "Unable to affect '$USER_UID' user to the '$GROUP_ID' group"
        echo "$USERNAME user affected to the '$GROUP_ID' group."
    fi
}

function createRealm()
{
    
    #########################################
    # Login
    #########################################

    # see : http://www.keycloak.org/docs/3.1/server_admin/topics/admin-cli.html
    $kcadm config credentials --server $KEYCLOAK_URL --realm master --user $KEYCLOAK_USER --password $KEYCLOAK_PASSWORD
    if [ "$?" -ne 0 ]
    then
        echo "Unable to login"
        return 1
    else
        echo "logged as $KEYCLOAK_USER"
    fi
    
    $kcadm get realms/$realm 1> /dev/null
    if [ $? = 0 ]
    then
        echo "Realm '$realm' already exists. Abort configuration."
        return 0
    else
        echo "Realm '$realm' to be created."
    fi
    
    #########################################
    # Create realm
    #########################################
    REALM_ID=$($kcadm create realms -s realm=$realm -s enabled=true -i)
    if [ "$?" -ne 0 ]
    then
        echo "Unable to create realm"
        return 2
    else
        echo "Realm '$REALM_ID' created."
    fi
    
    $kcadm update realms/$realm -s registrationAllowed=true -s rememberMe=true
    if [ "$?" -ne 0 ]
    then
        echo "Unable to configure realm"
        return 3
    else
        echo "Realm '$REALM_ID' configured."
    fi
    
    return 0
}

function createRealm()
{
    #########################################
    # Login
    #########################################
    
    # see : http://www.keycloak.org/docs/3.1/server_admin/topics/admin-cli.html
    $kcadm config credentials --server $KEYCLOAK_URL --realm master --user $KEYCLOAK_USER --password $KEYCLOAK_PASSWORD
    if [ "$?" -ne 0 ]
    then
        echo "Unable to login"
        return 1
    else
        echo "logged as $KEYCLOAK_USER"
    fi
    
    $kcadm get realms/$realm 1> /dev/null
    if [ $? = 0 ]
    then
        echo "Realm '$realm' already exists. Abort configuration."
        return 0
    else
        echo "Realm '$realm' to be created."
    fi
    
    #########################################
    # Create realm
    #########################################
    REALM_ID=$($kcadm create realms -s realm=$realm -s enabled=true -i)
    if [ "$?" -ne 0 ]
    then
        echo "Unable to create realm"
        return 2
    else
        echo "Realm '$REALM_ID' created."
    fi
    
    $kcadm update realms/$realm -s registrationAllowed=true -s rememberMe=true
    if [ "$?" -ne 0 ]
    then
        echo "Unable to configure realm"
        return 3
    else
        echo "Realm '$REALM_ID' configured."
    fi
    
    return 0
}

function exportRealmKeys()
{
    echo "clean ouput keys..."
    rm $output/{*.pem,*.json} 2> /dev/null
    [ $? = 0 ] && echo "Output directory cleaned!"
    
    echo "Get realm keys..."
    $kcadm get keys -r $realm >$output/keys.json
    [ $? = 0 ] || die "Unable to get realm keys"
    cat $output/keys.json
    
    jq ".keys[0].publicKey" -r $output/keys.json > $output/pub.tmp
    sed -e "1 i -----BEGIN PUBLIC KEY-----" -e "$ a -----END PUBLIC KEY-----" $output/pub.tmp > $output/pub.pem
    rm $output/pub.tmp
    jq ".keys[0].certificate" -r $output/keys.json > $output/cert.tmp
    sed -e "1 i -----BEGIN CERTIFICATE-----" -e "$ a -----END CERTIFICATE-----" $output/cert.tmp > $output/cert.pem
    rm $output/cert.tmp
}

echo "KEYCLOAK_USER=$KEYCLOAK_USER"
echo "KEYCLOAK_PASSWORD=$KEYCLOAK_PASSWORD"
echo "KEYCLOAK_URL=$KEYCLOAK_URL"
echo "KC_REALM_NAME=$KC_REALM_NAME"
echo "KC_REALM_DISPLAY_NAME=$KC_REALM_DISPLAY_NAME"
echo "KC_REALM_USERNAME=$KC_REALM_USERNAME"
echo "KC_REALM_PASSWORD=$KC_REALM_PASSWORD"
echo "KC_API_CLIENT_ID=$KC_API_CLIENT_ID"
echo "KC_API_CLIENT_HOSTNAME_PUBLIC=$KC_API_CLIENT_HOSTNAME_PUBLIC"
echo "KC_API_CLIENT_HOSTNAME_PUBLIC_IP=$KC_API_CLIENT_HOSTNAME_PUBLIC_IP"
echo "KC_PUBLIC_HOSTNAME_PUBLIC=$KC_PUBLIC_HOSTNAME_PUBLIC"

waitUrl "$KEYCLOAK_URL"
    
kcadm=$JBOSS_HOME/bin/kcadm.sh
output=/opt/jboss/keycloak
mkdir -p "$output"

realm=$KC_REALM_NAME
if [ -z "$realm" ]
then
    echo "Realm not set. Beware to call this script with Make!"
    exit 1
fi

if createRealm
then
    exit $?
fi

if getRealm
then
    exit $?
fi

exportRealmKeys

#########################################
# Create roles
#########################################
createRole "ROLE_ADMIN" "description=Regular admin with full set of permissions"
createRole "ROLE_OPERATOR" "description=Regular operator with basic set of permissions"
createRole "ROLE_INSTALLER_MODULE" "description=Regular technician with only enrollement permissions"

#########################################
# Create groups
#########################################
GROUP_ID=""
createGroup "AdminZephir"
gitAdmin=$GROUP_ID
createGroup "OperatorZephir"
gidOperator=$GROUP_ID
createGroup "InstallerZephir"
gidInstaller=$GROUP_ID

#########################################
# Role affectation
#########################################
affectGroupToRole "AdminZephir" "ROLE_ADMIN"
affectGroupToRole "OperatorZephir" "ROLE_OPERATOR"
affectGroupToRole "InstallerZephir" "ROLE_INSTALLER_MODULE"

#########################################
# Create users
#########################################
USER_UID=""
createUser "$KC_REALM_USERNAME" "$KC_REALM_PASSWORD" "$gitAdmin"
uid=$USER_UID
createUser "gilles" "eole" "$gidOperator"
uid_gilles=$USER_UID
createUser "gwen" "eole" "$gidOperator"
uid_gwen=$USER_UID
createUser "test" "test" "$gidInstaller"
uid_test=$USER_UID


#########################################
# Create client(s)
#########################################

client_id=$($kcadm create clients \
  -r $realm \
  -s clientId=$KC_API_CLIENT_ID \
  -s baseUrl=$KC_API_CLIENT_BASEURL \
  -s "redirectUris=[\"$KC_API_CLIENT_BASEURL/*\",\"$KC_API_CLIENT_BASEURL_IP/*\"]" \
  -s "webOrigins=[\"+\"]" \
  -i)
[ $? = 0 ] || die "Unable to create client"

echo "Client '$client_id' created."

#########################################
# Getting adapter configuration file
#########################################
echo "Get adapter configuration file..."
$kcadm get clients/$client_id/installation/providers/keycloak-oidc-keycloak-json \
  -r $realm \
  | jq ".[\"auth-server-url\"]=\"$KC_PUBLIC_BASEURL/auth\"" \
  > $output/keycloak.json
[ $? = 0 ] || die "Unable to get configuration file"
cat $output/keycloak.json

echo "Keycloak successfully configured."

SECRET=$(jq -r .credentials.secret ./keycloak.json)
echo "SECRET=$SECRET"

#echo "Now we are able to add a plugin to the new API. Let's activate the OIDC plugin:"
#curl -s -i --output /tmp/curl -X POST \
#  --url $KONG_API_URL/apis/test/plugins/ \
#  --data "name=oidc" \
#  --data "config.discovery=http://$IP_PUBLIC:8080/auth/realms/zephir/.well-known/openid-configuration" \
#  --data "config.client_id=zephir-api" \
#  --data "config.client_secret=$SECRET"
#  --data 'config.secret_is_base64=true'
#curlToJson
#testCurl "201"
#TEST_ID=$(jq -r .id /tmp/json)
