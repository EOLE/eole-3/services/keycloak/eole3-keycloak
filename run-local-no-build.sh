#!/bin/bash 
set -e

echo "REDEPLOIE=$1"
# shellcheck disable=SC1091
source ./function.sh

VERSION="$(getVersionArtifact)"
echo "$VERSION"

KEYCLOAK_CONTAINER=eole3-keycloak
BASE_IMAGE="hub.eole.education/test/eole3-keycloak"
IMAGE="${BASE_IMAGE}:${VERSION}"

if [ "$HASH_TAG" != "$(cat /tmp/${KEYCLOAK_CONTAINER}.hash 2>/dev/null)" ]
then
    echo "Hash différent -> redéploie"
    REDEPLOIE=oui
    CT_ID=$(docker ps |awk '/'${KEYCLOAK_CONTAINER}'/ {print $1;}')
    echo "CT_ID=$CT_ID"
    if [ -n "$CT_ID" ]
    then
        echo "container démarré -> stop $CT_ID"
        if [ "$REDEPLOIE" == oui ]
        then
            echo "stop $CT_ID"
            docker stop "$CT_ID"
            docker rm "$CT_ID"
        fi
    else
        echo "container non démarré -> déploie"
    fi
else
    echo "Hash indentique -> pas de redéploie"
fi
if docker ps |grep -q "${KEYCLOAK_CONTAINER}" >/dev/null
then
    echo "container démarré -> ok"
else
    echo "container non démarré -> déploie"
    REDEPLOIE=oui
fi
if [ "$REDEPLOIE" == oui ]
then
    echo "différent -> redéploie"
    dockerCleanAll
    
    if ! docker inspect keycloak-network >/dev/null 2>&1
    then
        echo "* création keycloak-network"
        docker network create keycloak-network
    else
        echo "* réseau déjà crée"
    fi

    if [ -z "$(docker ps -a --format '{{.ID}}' --filter 'name=keycloak-database')" ]
    then
        echo "* start keycloak-database"
        docker run -d --name keycloak-database \
            --net keycloak-network \
            -p 5432:5432 \
            -e "POSTGRES_USER=keycloak" \
            -e "POSTGRES_PASSWORD=keycloak" \
            -e "POSTGRES_DB=keycloak" \
            -e "PGDATA=/var/lib/postgresql/data" \
            -e "POSTGRES_INITDB_ARGS=--data-checksums" \
            postgres:12.0
    else
        echo "* keycloak-database déjà démarrée"
    fi
    waitConteneur keycloak-database "pg_isready"
    
    #docker stop "${KEYCLOAK_CONTAINER}" || /bin/true
    #docker rm "${KEYCLOAK_CONTAINER}" || /bin/true

    docker run --name "${KEYCLOAK_CONTAINER}" \
            --net keycloak-network \
           -p 8080:8080 \
           -e "JAVA_OPTS=-Dquarkus.transaction-manager.enable-recovery=true" \
           -e "KEYCLOAK_ADMIN=admin" \
           -e "KEYCLOAK_ADMIN_PASSWORD=admin" \
           -e "KEYCLOAK_LOGLEVEL=INFO" \
           -e "KEYCLOAK_WELCOME_THEME=keycloak" \
           -e "KEYCLOAK_DEFAULT_THEME=apps-theme" \
           -e "ROOT_LOGLEVEL=DEBUG" \
           -e "DB_VENDOR=POSTGRES" \
           -e "DB_ADDR=keycloak-database" \
           -e "DB_PORT=5432" \
           -e "DB_DATABASE=keycloak" \
           -e "DB_USER=keycloak" \
           -e "DB_PASSWORD=keycloak" \
           -e KEYCLOAK_USER=admin \
           -e KEYCLOAK_PASSWORD=admin \
           "$IMAGE" \
           start-dev \
           --hostname=localhost \
           --hostname-strict=false \
           --health-enabled=true \
           --metrics-enabled=true \
           --db=postgres --db-url-host=keycloak-database --db-username=keycloak --db-password=keycloak \
           --features=token-exchange,scripts \
           --http-relative-path=/auth \
           &
           #> "${KEYCLOAK_CONTAINER}.log"  &
#kc.sh start --optimized --proxy=edge --hostname=mykeycloak.127.0.0.1.nip.io --hostname-admin-url=http://mykeycloakadmin.127.0.0.1.nip.io:1234
           #-e KEYCLOAK_IMPORT=/tmp/realm-export.json \
    echo "$HASH_TAG" >/tmp/"${KEYCLOAK_CONTAINER}".hash
    echo "attente démarrage 30s"
    sleep 30
else
    echo "identique pas de déploie"
fi
waitConteneur "${KEYCLOAK_CONTAINER}"

# attente : Keycloak 15.0.2 (WildFly Core 15.0.1.Final) started in 20195ms - Started 1079 of 1373 services (713 services are lazy, passive or on-demand)
waitUrl http://localhost:8080/auth 10
waitUrl http://localhost:8080/auth/realms/master/.well-known/openid-configuration 1

if ! initToolsKcAdm
then
   exit 1
fi

#########################################
# login realm
#########################################
if ! loginKeycloack
then
   exit 1
fi
# actualise le cache users !
getUsers
getUserId "admin"

#KEYCLOAK_REALM=test
#createRealm
#waitUrl http://localhost:8080/auth/realms/test/.well-known/openid-configuration 1

#########################################
# groups
#########################################
getGroups
gidAdmin=$(getGroupId admin)

#########################################
# users
#########################################
getUsers
createUser "gilles" "eole" "$gidAdmin"
createUser "test" "test" "$gidAdmin"
createUser " test" "test" "$gidAdmin"

# actualise le cache users !
getUserId "gilles"
getUserId "test"
getUserId " test"

getClients

firefox "http://localhost:8080/auth/admin/master/console/#/realms/master"

firefox http://localhost:8080/auth/realms/master/protocol/openid-connect/auth?client_id=security-admin-console
