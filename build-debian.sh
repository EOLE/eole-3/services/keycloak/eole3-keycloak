#!/bin/bash

# shellcheck disable=SC1091
source ./function.sh

VERSION="$(getVersionArtifact)"
echo "VERSION=$VERSION"
VERSION_EOLE="${VERSION}-debian"
echo "VERSION_EOLE=$VERSION_EOLE"
DOCKERFILE="Dockerfile-debian"
LOCAL_TAG="eole3-keycloak:dev-debian"

VERSION_DOCKER=$(sed -n '/hub.eole.education\/test\/keycloak/p'  <"$DOCKERFILE" | sed -e 's/FROM.*://' -e 's/\r//' )
if [ -z "$VERSION_DOCKER" ]
then
  exit 1
fi
echo "$VERSION_DOCKER"

bash ./build_local.sh 

loginHarbor

BASE_IMAGE="hub.eole.education/test/eole3-keycloak"
IMAGE="${BASE_IMAGE}:${VERSION_EOLE}"

export BUILDKIT_PROGRESS=plain
#  --no-cache 
if ! docker build -f "$DOCKERFILE" . -t "$LOCAL_TAG" 
then
  exit 1
fi

SIZE=$(docker inspect "$LOCAL_TAG"  | jq '.[] | .Size')
echo "TAILLE=$SIZE"

if ! docker tag "$LOCAL_TAG" "$IMAGE"
then
  exit 1
fi

if ! docker push "$IMAGE"
then
  exit 1
fi

echo "Pour tester:"
echo "docker run -p 8080:8080 -e DB_VENDOR=H2 -e KEYCLOAK_USER=gilles -e KEYCLOAK_PASSWORD=gilles -e KEYCLOAK_ADMIN=admin -e KEYCLOAK_ADMIN_PASSWORD=admin $IMAGE start-dev "
echo "docker run -p 8080:8080 -e DB_VENDOR=H2 -e KEYCLOAK_USER=gilles -e KEYCLOAK_PASSWORD=gilles -e KEYCLOAK_ADMIN=admin -e KEYCLOAK_ADMIN_PASSWORD=admin $LOCAL_TAG start-dev "
