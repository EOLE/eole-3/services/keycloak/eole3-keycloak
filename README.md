## Image Keycloak avec extension Apps

Cette image est issue de l'image officielle Keycloak à laquelle plusieurs extensions ont été ajoutées. 
Elles viennent de : https://www.keycloak.org/extensions.html

# plugins ajoutés

    plugin KUBE_PING
    Keycloak-Apps-Themes
    Keycloak Mail domain Check
    Keycloak Auth Decorator
    Keycloak Json Remote Claim
    Keycloak FranceConnect
    Keycloak Metrics Spi

# préparation

    
    requirement fonctionnel : ~/.gitlab/token avec
        HARBOR_USERNAME=xxxxxxxxxxxxx
        HARBOR_TOKEN=xxxxxxxx
        GITLAB_TOKEN=xxxxxxxxx

# Pour construire 'hub.eole.education/test/keycloak:v20.0xxx'

    git clone https://gitlab.mim-libre.fr/EOLE/eole-3/services/keycloak/eole3-keycloak.git
    git pull
    git checkout v20.0
    cd keycloak
    bash build.sh
    # créer keycloak:v20.0 ...

# Pour construire 'hub.eole.education/test/eole3-keycloak:v20.0xxx'

    git clone https://gitlab.mim-libre.fr/EOLE/eole-3/services/keycloak/eole3-keycloak.git
    git pull
    git checkout v20.0
    bash build.sh
    # créer eole3-keycloak:v20.0 from keycloak:v20.0 avec les plugins !
    
= Voir aussi

    Eole Keycloak Containers (plus utiliser depuis > v19)
