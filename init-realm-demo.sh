#!/bin/bash 
set -e

# shellcheck disable=SC1091
source ./function.sh

KEYCLOAK_CONTAINER=eole3-keycloak

# attente : Keycloak 15.0.2 (WildFly Core 15.0.1.Final) started in 20195ms - Started 1079 of 1373 services (713 services are lazy, passive or on-demand)
waitUrl http://192.168.230.44:8080/ 10
waitUrl http://192.168.230.44:8080/auth/realms/master/.well-known/openid-configuration 1

initToolsKcAdm

#########################################
# login realm
#########################################
if ! loginKeycloack
then
   exit 1
fi

#########################################
# groups
#########################################
getGroups
gidAdmin=$(getGroupId admin)

#########################################
# users
#########################################
getUsers
createUser "gilles" "eole" "$gidAdmin"
createUser "test" "test"
createUser "_test" "test"

# actualise le cache users !
getUserId "admin"
getUserId "gilles"
getUserId "test"

firefox http://localhost:8080/auth/realms/master/protocol/openid-connect/auth?client_id=security-admin-console
