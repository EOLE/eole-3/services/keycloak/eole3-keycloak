FROM registry.access.redhat.com/ubi9 AS ubi-micro-build

ENV KEYCLOAK_VERSION 22.0.5
ARG KEYCLOAK_DIST=https://github.com/keycloak/keycloak/releases/download/$KEYCLOAK_VERSION/keycloak-$KEYCLOAK_VERSION.tar.gz

RUN dnf install -y tar gzip

ADD $KEYCLOAK_DIST /tmp/keycloak/

# The next step makes it uniform for local development and upstream built.
# If it is a local tar archive then it is unpacked, if from remote is just downloaded.
RUN (cd /tmp/keycloak && \
    tar -xvf /tmp/keycloak/keycloak-*.tar.gz && \
    rm /tmp/keycloak/keycloak-*.tar.gz) || true

RUN mv /tmp/keycloak/keycloak-* /opt/keycloak && mkdir -p /opt/keycloak/data
RUN chmod -R g+rwX /opt/keycloak

ADD keycloak/ubi-null.sh /tmp/
RUN bash /tmp/ubi-null.sh java-17-openjdk-headless glibc-langpack-en

COPY libs/*.jar opt/keycloak/providers/

#################################################
#FROM hub.eole.education/test/keycloak:22.0.5-eole3.1
FROM registry.access.redhat.com/ubi9-micro
ENV LANG en_US.UTF-8

COPY --from=ubi-micro-build /tmp/null/rootfs/ /
COPY --from=ubi-micro-build --chown=1000:0 /opt/keycloak /opt/keycloak

#RUN /opt/keycloak/bin/kc.sh build --health-enabled=true --metrics-enabled=true --db=postgres --features=token-exchange,scripts --http-relative-path=/auth
RUN /opt/keycloak/bin/kc.sh build --health-enabled=true --db=postgres --features=token-exchange,scripts --http-relative-path=/auth

# Installed features: [agroal, cdi, hibernate-orm, jdbc-h2, jdbc-mariadb, jdbc-mssql, jdbc-mysql, jdbc-oracle, jdbc-postgresql, keycloak, logging-gelf, micrometer, narayana-jta, reactive-routes, resteasy, resteasy-jackson, smallrye-context-propagation, smallrye-health, vertx]

RUN echo "keycloak:x:0:root" >> /etc/group && \
    echo "keycloak:x:1000:0:keycloak user:/opt/keycloak:/sbin/nologin" >> /etc/passwd

USER 1000

EXPOSE 8080
EXPOSE 8443

#ENV KEYCLOAK_ADMIN=admin
#ENV KEYCLOAK_ADMIN_PASSWORD=admin
#ENV KC_DB_URL=<DBURL>
#ENV KC_DB_USERNAME=<DBUSERNAME>
#ENV KC_DB_PASSWORD=<DBPASSWORD>
#ENV KC_HOSTNAME=localhost:8443
#ENV JAVA_OPTS_APPEND "-Djava.security.egd=file:/dev/./urandom -Dlog4j.formatMsgNoLookups=true -XX:+UnlockExperimentalVMOptions -XX:+EnableJVMCI -XshowSettings:vm" 
#ENV JAVA_OPTS_APPEND "-Djava.security.egd=file:/dev/./urandom -Dlog4j.formatMsgNoLookups=true -XX:+UnlockExperimentalVMOptions -XX:+EnableJVMCI"
ENTRYPOINT [ "/opt/keycloak/bin/kc.sh" ]
