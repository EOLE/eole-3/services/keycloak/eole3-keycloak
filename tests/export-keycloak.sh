die() { echo "$@" 1>&2 ; exit 1; }

function waitUrl()
{
    local URL="$1"
    local NB="${2:-30}"
    #echo "waitUrl: $URL"
    while [ "${NB}" -gt 0 ] ; do
      if curl --output /tmp/curl --silent --head "$URL"
      then
          HTTPCODE=$(awk 'NR==1 { print $2; }' /tmp/curl)
      else
          HTTPCODE="-1"
      fi
           
      if [ "$HTTPCODE" = "200" ] || [ "$HTTPCODE" = "303" ] #|| [ "$HTTPCODE" = "404" ]
      then
          return 0
      fi
      sleep 1
      NB=$(( NB - 1))
    done
    cat /tmp/curl
    return 1
}

kcadm=$JBOSS_HOME/bin/kcadm.sh
output=/opt/jboss/keycloak
mkdir -p "$output"

realm=$KC_REALM_NAME
[ -z "$realm" ] && die "Realm not set. Beware to call this script with Make!"

waitUrl "$KEYCLOAK_URL"

# see : http://www.keycloak.org/docs/3.1/server_admin/topics/admin-cli.html
$kcadm config credentials --server $KEYCLOAK_URL --realm master --user $KEYCLOAK_USER --password $KEYCLOAK_PASSWORD
$kcadm get realms/$realm 1>$output/realm_$realm.json
$kcadm get keys -r $realm >$output/keys.json
$kcadm get roles -r $realm
#$kcadm get roles/ROLE_ADMIN_ZEPHIR -r $realm >$output/role_admin_zephir.json 
#$kcadm get roles/ROLE_OPERATOR_ZEPHIR -r $realm >$output/role_operator_zephir.json 
#$kcadm get roles/ROLE_INSTALLER_MODULE -r $realm >$output/role_installer_module.json 
$kcadm get groups -r $realm
#$kcadm get groups/AdminZephir -r $realm >$output/group_admin_zephir.json
#$kcadm get groups/OperatorZephir -r $realm >$output/group_operator_zephir.json
#$kcadm get groups/InstallerZephir -r $realm >$output/group_installer_module.json
$kcadm get clients/$KC_API_CLIENT_ID -r $realm
$kcadm get clients -r $realm $KC_API_CLIENT_ID >$output/clients.json
$kcadm get clients/$KC_API_CLIENT_ID/installation/providers/keycloak-oidc-keycloak-json -r $realm >$output/clients_oidc.json
