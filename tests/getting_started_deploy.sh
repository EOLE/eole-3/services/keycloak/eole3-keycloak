#!/bin/bash

set -e

VERSION=$(xmllint --xpath "//*[local-name()='project']/*[local-name()='version']/text()" ../pom.xml)
if [ -z "$VERSION" ]
then
  exit 1
fi

IMAGE_KEYCLOACK="hub.eole.education/test/eole3-keycloak:$VERSION"

# shellcheck disable=SC1091
source ./function.sh

#KEYCLOAK_URL="http://eolebase.ac-test.fr/sso"
KEYCLOAK_URL="http://localhost:8080"
echo "KEYCLOAK_URL=$KEYCLOAK_URL"

echo "* start keycloak-database"
if ! docker inspect keycloak-database >/dev/null 2>&1
then
    if ! docker inspect keycloak-network >/dev/null 2>&1
    then
        docker network create keycloak-network
    fi
    
    docker run -d --name keycloak-database \
        --net keycloak-network \
        -p 5433:5432 \
        -e "POSTGRES_USER=keycloak" \
        -e "POSTGRES_PASSWORD=keycloak" \
        -e "POSTGRES_DB=keycloak" \
        -e "PGDATA=/var/lib/postgresql/data" \
        -e "POSTGRES_INITDB_ARGS=--data-checksums" \
        postgres:12.0

fi
waitConteneur keycloak-database "pg_isready"

echo "* start keycloak 1"
if ! docker inspect keycloak1 >/dev/null 2>&1
then
    docker run --name keycloak1 \
        --net keycloak-network \
        -e "JAVA_OPTS=" \
        -e "KEYCLOAK_ADMIN=admin" \
        -e "KEYCLOAK_ADMIN_PASSWORD=admin" \
        -e "KEYCLOAK_LOGLEVEL=INFO" \
        -e "ROOT_LOGLEVEL=INFO" \
        -e "DB_VENDOR=POSTGRES" \
        -e "DB_ADDR=keycloak-database" \
        -e "DB_PORT=5432" \
        -e "DB_DATABASE=keycloak" \
        -e "DB_USER=keycloak" \
        -e "DB_PASSWORD=keycloak" \
        -p 8080:8080 \
        "$IMAGE_KEYCLOACK" \
        start \
        &
        -e "JAVA_OPTS_APPEND=-Djava.security.egd=file:/dev/./urandom -Dlog4j.formatMsgNoLookups=true -XX:+UnlockExperimentalVMOptions -XX:MaxRAMFraction=1 -XshowSettings:vm" \
#        -e "JAVA_OPTS_APPEND=-Djava.security.egd=file:/dev/./urandom -Dlog4j.formatMsgNoLookups=true -XX:+UnlockExperimentalVMOptions -XX:MaxRAMFraction=1 -XshowSettings:vm" \
#        -e "JAVA_OPTS=-server -Xms256m -Xmx2048m -XX:MetaspaceSize=96M -XX:MaxMetaspaceSize=256m -Djava.net.preferIPv4Stack=true -Djboss.modules.system.pkgs=org.jboss.byteman -Djava.awt.headless=true " \
#        -e "PROXY_ADDRESS_FORWARDING=true" \
#        -e "KEYCLOAK_FRONTEND_URL=$KEYCLOAK_URL" \
# KEYCLOAK_HOSTNAME
# KEYCLOAK_HTTP_PORT
# KEYCLOAK_HTTPS_PORT
# KEYCLOAK_ALWAYS_HTTPS
# KEYCLOAK_IMPORT <fichier>
# BIND
# BIND_OPTS
# ROOT_LOGLEVEL
# KEYCLOAK_STATISTICS
    waitConteneur keycloak1
    sleep 10
fi

echo "* start keycloak 2"
#if ! docker inspect keycloak2 >/dev/null 2>&1
#then
#    docker run --name keycloak2 \
#        --net keycloak-network \
#        -e "KEYCLOAK_LOGLEVEL=INFO" \
#        -e "ROOT_LOGLEVEL=INFO" \
#        -e "PROXY_ADDRESS_FORWARDING=true" \
#        -e "DB_VENDOR=POSTGRES" \
#        -e "DB_ADDR=keycloak-database" \
#        -e "DB_PORT=5432" \
#        -e "DB_DATABASE=keycloak" \
#        -e "DB_USER=keycloak" \
#        -e "DB_PASSWORD=keycloak" \
#        -e "JAVA_OPTS=-server -Xms256m -Xmx2048m -XX:MetaspaceSize=96M -XX:MaxMetaspaceSize=256m -Djava.net.preferIPv4Stack=true -Djboss.modules.system.pkgs=org.jboss.byteman -Djava.awt.headless=true " \
#        -e "JAVA_OPTS_APPEND=-Djava.security.egd=file:/dev/./urandom -Dlog4j.formatMsgNoLookups=true -XX:+UnlockExperimentalVMOptions -XX:MaxRAMFraction=1 -XshowSettings:vm" \
#        -p 8081:8080 \
#        "$IMAGE_KEYCLOACK" \
#        &
#
#    waitConteneur keycloak2
#        -e "KEYCLOAK_FRONTEND_URL=$KEYCLOAK_URL" \
#    sleep 10
#fi

# attente : Keycloak 4.5.0.Final (WildFly Core 5.0.0.Final) started in 38989ms
waitUrl $KEYCLOAK_URL/ 60
waitUrl $KEYCLOAK_URL/auth/realms/master/.well-known/openid-configuration 1

