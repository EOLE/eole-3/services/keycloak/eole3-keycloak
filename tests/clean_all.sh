#!/bin/bash -x

docker container list 

docker image list -q --filter 'dangling=true' | xargs -t --no-run-if-empty docker rmi
    
docker container rm -f $(docker ps -aq) 
docker volume prune -f
docker image prune -f

# nettoyage comple
docker system prune --all --force
