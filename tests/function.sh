#!/bin/bash

if ! command -v jq >/dev/null 2>&1
then
    apt install -y jq
fi

if ! command -v openssl >/dev/null 2>&1
then
    apt install -y openssl
fi

if ! command -v curl >/dev/null 2>&1
then
    apt install -y curl
fi

if [ -f .env ]
then
    # shellcheck disable=SC1091
    source .env
fi

if [ -z "$HOSTNAME_PUBLIC" ]
then
    HOSTNAME_PUBLIC="$(hostname)"
fi
echo "HOSTNAME_PUBLIC=$HOSTNAME_PUBLIC"

echo "IP_PUBLIC=$IP_PUBLIC"
EXPORTKEYS_PATH="$HOME/exportkeys"
export EXPORTKEYS_PATH

PUBLIC_URL="http://$HOSTNAME_PUBLIC"
echo "PUBLIC_URL=$PUBLIC_URL"

KEYCLOAK_URL="http://$HOSTNAME_PUBLIC:8080"
echo "KEYCLOAK_URL=$KEYCLOAK_URL"

function displayCurl()
{
    if [ -f /tmp/curl ]
    then
       cat /tmp/curl
       rm -f /tmp/curl
    fi
}

function testCurl()
{
    if [ ! -f /tmp/curl ]
    then
        return 1
    fi
    HTTPCODE=$(awk 'NR==1 { print $2; }' /tmp/curl)
    while (( "$#" )); do
        if [[ "$1" == "$HTTPCODE" ]];
        then
            echo "  > $HTTPCODE : OK"
            return 0
        fi
        shift
    done
    echo "  > $HTTPCODE : NOK"
    displayCurl
    exit 1
}

function waitConteneur()
{
    #echo "waitConteneur: $*"
    CONTENEUR="${1}"
    shift
    n=15;
    while [ "${n}" -gt 0 ] ; do
            status=$(docker inspect --format "{{json .State.Status }}" "${CONTENEUR}" )
            if [ -z "${status}" ];
            then
                echo "waitConteneur ${CONTENEUR} : No status informations.";
                return 1
            fi;
            #echo "Waiting for ${CONTENEUR} up and ready (${status})...";
            if [ "\"running\"" = "${status}" ];
            then
                if [[ -z "$*" ]]
                then
                    return 0
                fi
                if docker exec "${CONTENEUR}" "$@"
                then
                    return 0
                #else
                #    echo "docker => $?"
                fi
            fi
            sleep 2;
            n=$(( n - 1))
    done;
    echo "Waiting for ${CONTENEUR} : TIMEOUT, stop !";
    return 1
}

function waitUrl()
{
    local URL="$1"
    shift
    local NB="${1:-30}"
    shift
    echo "waitUrl: $URL $NB $*"
    rm -f /tmp/curl
    while [ "${NB}" -gt 0 ] ; do
      curl --output /tmp/curl --silent --head "$URL" && CDU="$?" || CDU="$?" 
      
      if [ "$CDU" = "0" ] 
      then
         if [[ -z "$*" ]]
         then
             if testCurl "200" "401" "303"
             then
                return 0
             fi
         else
             if testCurl "$@"
             then
                return 0
             fi
         fi
      fi
      #if [ "$CDU" = "22" ]
      #then
         # 22 : HTTP page not retrieved. The requested url was not found or returned another error with the HTTP error code being 400 or above. 
         #      This return code only appears if -f, --fail is used.
      #fi
      #if [ "$CDU" = "7" ]
      #then
         # 7 :  Failed to connect to host. curl managed to get an IP address to the machine and it tried to setup a TCP connection to the host but failed.
         #      This can be because you have specified the wrong port number, entered the wrong host name, the wrong protocol or perhaps because there is 
         #      a firewall or another network equipment in between that blocks the traffic from getting through.
      #fi
      sleep 1
      NB=$(( NB - 1))
      echo "$NB : $CDU"
    done
    if testCurl "200"
    then
       return 0
    fi
    displayCurl
    return 1
}

function waitUrlHttp()
{
    local URL="$1"
    local NB="${2:-30}"
    #echo "waitUrl: $URL"
    rm -f /tmp/curl
    while [ "${NB}" -gt 0 ] ; do
      curl --output /tmp/curl --silent --head --fail "$URL" && CDU="$?" || CDU="$?" 
      if [ "$CDU" = "0" ] || [ "$CDU" = "22" ] || [ "$CDU" = "7" ]
      then
          HTTPCODE=$(awk 'NR==1 { print $2; }' /tmp/curl)
      else
          HTTPCODE="-1"
      fi
           
      if [ "$HTTPCODE" = "200" ] || [ "$HTTPCODE" = "303" ] #|| [ "$HTTPCODE" = "404" ]
      then
          return 0
      fi
      sleep 1
      NB=$(( NB - 1))
    done
    displayCurl
    return 1
}


base64_encode()
{
    declare input=${1:-$(</dev/stdin)}
    printf '%s' "${input}" | openssl enc -base64 -A
}

base64_decode()
{
    declare input=${1:-$(</dev/stdin)}
    printf '%s' "${input}" | openssl enc -base64 -d -A
}

json() {
    declare input=${1:-$(</dev/stdin)}
    printf '%s' "${input}" | jq -c .
}

function curlToJson()
{
    # creation de /tmp/json
    grep "{" /tmp/curl >/tmp/json

    # affichage du json
    jq -M '' </tmp/json
}

