#!/bin/bash 
set -e

# shellcheck disable=SC1091
source ./function.sh

VERSION="$(getVersionArtifact)"
VERSION="${VERSION/-eole3/-legacy-eole3}"
echo "$VERSION"

loginHarbor

KEYCLOAK_CONTAINER=eole3-keycloak
BASE_IMAGE="hub.eole.education/test/eole3-keycloak"
IMAGE="${BASE_IMAGE}:${VERSION}"

HASH_TAG=$(docker build -q -f Dockerfile-legacy .)
echo "HASH_TAG=$HASH_TAG"
REDEPLOIE=non
if [ "$HASH_TAG" != "$(cat /tmp/${KEYCLOAK_CONTAINER}-legacy.hash 2>/dev/null)" ]
then
    echo "Hash différent -> redéploie"
    REDEPLOIE=oui
    if ! docker build -t "$IMAGE" .
    then
        exit 1
    fi
fi
if docker ps |grep -q "${KEYCLOAK_CONTAINER}" >/dev/null
then
    echo "container démarré -> ok"
else
    echo "container non démarré -> déploie"
    REDEPLOIE=oui
fi
if [ "$REDEPLOIE" == oui ]
then
    echo "différent -> redéploie"
    dockerCleanAll
    docker run --name "${KEYCLOAK_CONTAINER}" \
           -p 8080:8080 \
           -e DB_VENDOR=H2 \
           -e KEYCLOAK_USER=admin \
           -e KEYCLOAK_PASSWORD=admin \
           -e KEYCLOAK_WELCOME_THEME=keycloak \
           -e KEYCLOAK_DEFAULT_THEME=apps-theme \
           "$IMAGE" \
           > "${KEYCLOAK_CONTAINER}-legacy.log"  &

    echo "$HASH_TAG" >"/tmp/${KEYCLOAK_CONTAINER}-legacy.hash"
    sleep 20
else
    echo "identique pas de déploie"
fi
waitConteneur "${KEYCLOAK_CONTAINER}"

# attente : Keycloak 15.0.2 (WildFly Core 15.0.1.Final) started in 20195ms - Started 1079 of 1373 services (713 services are lazy, passive or on-demand)
waitUrl http://localhost:8080/ 10
waitUrl http://localhost:8080/auth/realms/master/.well-known/openid-configuration 1

initToolsKcAdm

#########################################
# login realm
#########################################
if ! loginKeycloack
then
   exit 1
fi

#########################################
# groups
#########################################
getGroups
gidAdmin=$(getGroupId admin)

#########################################
# users
#########################################
getUsers
createUser "gilles" "eole" "$gidAdmin"
createUser "test" "test" "$gidAdmin"

# actualise le cache users !
getUserId "admin"
getUserId "gilles"
getUserId "test"

firefox http://localhost:8080/auth/realms/master/protocol/openid-connect/auth?client_id=security-admin-console
